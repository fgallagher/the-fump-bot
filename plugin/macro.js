// Macros
var fs = require("fs");
var d3 = require("d3");

var macros = {};

var pluginRegex = /^(?:(?:!|\/)macros?)(?:(?: )(.*))*/i;
var macroCommandRegex = /^(?:\/)(\w+)$/i;

function Macro() {
	this.name = "Fump Macros";
	this.version = "1.0";

	macros = JSON.parse(fs.readFileSync("./macros.json"));
	
	this.handleMessage = function ( msg ) {
		if( pluginRegex.test(msg.content) ) {
			handleMacroRequest(msg, pluginRegex);
		} else {
			this.runMacro(msg);
		}
	};
	
	this.runMacro = function ( msg ) {
		if( macros.hasOwnProperty(msg.author.id) ) {
			if( macroCommandRegex.test(msg.content) ) {
				var command = macroCommandRegex.exec(msg.content)[1];
		
				if( macros[msg.author.id].hasOwnProperty(command) ) {
					var userHandle = "<@!" + msg.author.id + ">";

					// msg.channel.sendMessage( "_macro: " + command + " = " + macros[msg.author.id][command] + "_" );
					msg.content = macros[msg.author.id][command];
				}
			}
		}
	};
};

function handleMacroRequest( msg, regex ) {
	var macroArgs = regex.exec(msg.content);
	var username = msg.author.username;
	var userid = msg.author.id;
	var macroAction;
	var macroDetails;

	if( macroArgs[1] == undefined ) {
		macroAction = 'list';
	} else {
		macroAction = macroArgs[1].split(' ', 1)[0];
		macroDetails = macroArgs[1].split(' ').slice(1).join(' ');
	}

	if( ! macros.hasOwnProperty(userid) ) {
		macros[userid] = {};
	}

	switch (macroAction) {
		case "add":
			var macroKey = macroDetails.split(' ', 1)[0];
		
			if( macros[userid].hasOwnProperty(macroKey) ) {
				msg.channel.sendMessage("Macro _" + macroKey + "_ already exists.");
				break;
			}
		case "edit":
		case "replace":
			var macroKey = macroDetails.split(' ', 1)[0];
			var macroCommand = macroDetails.split(' ').slice(1).join(' ');
		
			macros[userid][macroKey] = macroCommand;
			fs.writeFileSync("macros.json", JSON.stringify(macros));

			msg.channel.sendMessage("Macro _" + macroKey + "_ added.");

			break;
		case "del":
		case "delete":
		case "remove":
		case "rm":
			var macroKey = macroDetails.split(' ', 1)[0];
		
			if( macros[userid].hasOwnProperty(macroKey) ) {
				delete macros[userid][macroKey];
				fs.writeFileSync("macros.json", JSON.stringify(macros));
		
				msg.channel.sendMessage("Macro _" + macroKey + "_ deleted.");
			} else {
				msg.channel.sendMessage("Macro _" + macroKey + "_ does not exist.");
			}
		
			break;
		case "list":
			var message = "**" + username + "'s Macros:**";

			if( macros.hasOwnProperty(userid) && d3.keys(macros[userid]).length > 0 ) {
				for( var key in macros[userid] ) {
					if( macros[userid].hasOwnProperty(key) ) {
						message += "\n\t**/" + key + "** : "+ macros[userid][key];
					}
				}
			} else {
				message += "You have no macros defined.";
			}

			msg.author.sendMessage(message);

			break;
		default:
			msg.channel.sendMessage("Invalid macro command.");
	}
};

module.exports = Macro;
