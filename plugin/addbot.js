// AD&D Bot
// Written by: Michael Puscar 01/24/2017
// Modified: 01/30/2017

// Commands:
//   
// {Ability based} 
// str dex con wis int cha 
//
// {Innate}
// perc
//
// {Action based}
// init

function ADDPlugin() {
	this.name = "AD&D Bot";
	this.version = "1.0";

	this.handleMessage = function ( msg ) {
		if (msg.author.bot) return;
		if (msg.author.username == "The Fump") return;
		if (!msg.content.startsWith("/")) return;

		var who = msg.author.username;
		var attmod = getModifier(msg); 
		if (attmod == null) return;

		if (attmod[1] == "-20") { 
		    msg.channel.sendMessage("You don't have a character in this game. Rolling without bonuses.");
		    attmod[1] = 0;
		}

		if (msg.content.indexOf("(") != -1) {
		    who = msg.content.substring(msg.content.lastIndexOf("(")+1, msg.content.lastIndexOf(")"));
		    who = toTitleCase(who);
		} else {
		    who = msg.author;
		}

		rollDice(msg, who, attmod[0], attmod[1]);
	};
}

module.exports = ADDPlugin;

function rollDice(msg, who, roll, modifier) {
    var randomnum = Math.floor(Math.random() * 20) + 1;
    var myroll = randomnum + parseInt(modifier);

    var mymsg = msg.channel.sendMessage(who+" makes a"+roll+" (1d20+"+modifier+"). The result is "+randomnum+" + "+modifier+" = "+myroll);
    if (randomnum == 1) { msg.channel.sendMessage("Oh no! CRITICAL FAILURE!"); }
    if (randomnum == 20) { msg.channel.sendMessage("OH YEAH! CRITICAL SUCCESS!"); }

    if (msg.content.startsWith("/init")) { 
	//mymsg.pin();
	//client.rest.methods.pinMessage(mymsg); 
    }
}

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function getModifier(msg) {
    if (msg.author.username == "Harry") {
	if (msg.content.startsWith("/str")) { return [" strength check", "-1"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "5"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "0"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "2"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "1"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "3"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "7"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "2"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "2"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "1"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "7"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "2"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "2"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "3"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "2"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "2"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "2"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "3"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "3"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "4"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "2"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "7"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "9"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "2"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "5"]; }

	if (msg.content.startsWith("/attack")) { return ["n attack roll (bow)", "7"]; }
	if (msg.content.startsWith("/shor")) { return ["n attack roll (short sword)", "7"]; }
	if (msg.content.startsWith("/dag")) { return ["n attack roll (dagger)", "7"]; }
    }

    else if (msg.author.username == "frithmyn") {
	if (msg.content.startsWith("/str")) { return [" strength check", "-1"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "2"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "1"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "1"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "1"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "-1"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "4"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "1"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "1"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "-1"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "-1"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "1"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "1"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "-1"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "1"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "1"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "1"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "-1"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "-1"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "1"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "1"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "2"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "4"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "1"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "2"]; }

	if (msg.content.startsWith("/attack")) { return ["n attack roll (quarterstaff)", "4"]; }
	if (msg.content.startsWith("/open")) { return ["n attack roll (open hand)", "2"]; }
	if (msg.content.startsWith("/dart")) { return ["n attack roll (dart)", "4"]; }
    }

    else if (msg.author.username == "Fark") {
	if (msg.content.startsWith("/str")) { return [" strength check", "0"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "-1"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "2"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "-1"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "3"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "2"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "-1"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "3"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "-1"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "0"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "4"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "1"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "5"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "2"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "-1"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "3"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "-1"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "2"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "4"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "3"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "-1"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "-1"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "-1"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "3"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "-1"]; }

	if (msg.content.startsWith("/attack")) { return ["n attack roll (warhammer)", "2"]; }
	if (msg.content.startsWith("/cros")) { return ["n attack roll (crossbow)", "1"]; }
	if (msg.content.startsWith("/ham")) { return ["n attack roll (War Priest Hammer)", "2"]; }
	if (msg.content.startsWith("/war")) { return ["n attack roll (warhammer)", "2"]; }
    }

    else if (msg.author.username == "Scott") {
	if (msg.content.startsWith("/str")) { return [" strength check", "4"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "2"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "2"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "2"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "1"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "1"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "4"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "1"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "2"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "6"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "3"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "2"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "1"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "1"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "2"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "1"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "2"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "1"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "1"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "1"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "2"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "2"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "4"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "1"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "2"]; }

	if (msg.content.startsWith("/attack")) { return ["n attack roll (short sword)", "6"]; }
	if (msg.content.startsWith("/short")) { return ["n attack roll (short sword)", "6"]; }
	if (msg.content.startsWith("/dagger")) { return ["n attack roll (dagger)", "6"]; }

    }
    else if (msg.content.includes("enseric")) {
	if (msg.content.startsWith("/str")) { return [" strength check", "1"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "1"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "0"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "0"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "0"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "1"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "3"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "0"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "0"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "1"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "1"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "0"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "0"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "1"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "0"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "0"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "0"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "1"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "1"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "2"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "0"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "1"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "3"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "1"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "1"]; }

	if (msg.content.startsWith("/attack")) { return ["n attack roll (Sword of Dwaaron)", "4"]; }
    }

    else if (msg.content.includes("max")) {
	if (msg.content.startsWith("/str")) { return [" strength check", "0"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "1"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "1"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "0"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "0"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "0"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "3"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "0"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "0"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "0"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "0"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "0"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "0"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "0"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "0"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "0"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "0"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "1"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "1"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "2"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "0"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "1"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "3"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "1"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "1"]; }

	if (msg.content.startsWith("/attack")) { return ["n attack roll (sword)", "3"]; }
    }

    else if (msg.content.includes("slave")) {
	if (msg.content.startsWith("/str")) { return [" strength check", "2"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "0"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "2"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "0"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "0"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "0"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "0"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "0"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "0"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "4"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "0"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "0"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "0"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "0"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "0"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "0"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "0"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "0"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "0"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "2"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "0"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "0"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "0"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "0"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "0"]; }

	if (msg.content.startsWith("/attack")) { return ["n attack roll (flail)", "4"]; }
    }

    else if (msg.content.includes("duer")) {
	if (msg.content.startsWith("/str")) { return [" strength check", "2"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "0"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "2"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "0"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "0"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "-1"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "0"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "0"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "0"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "4"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "-1"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "0"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "0"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "-1"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "0"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "0"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "0"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "0"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "0"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "2"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "0"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "0"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "2"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "0"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "0"]; }

	if (msg.content.startsWith("/attack")) { return ["n attack roll (war pick)", "4"]; }
	if (msg.content.startsWith("/jav")) { return ["n attack roll (javelin)", "4"]; }
    }

    else if (msg.content.includes("thug")) {
      	if (msg.content.startsWith("/str")) { return [" strength check", "0"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "0"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "0"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "0"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "0"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "0"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "0"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "0"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "0"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "0"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "0"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "0"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "0"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "0"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "0"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "0"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "0"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "0"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "0"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "0"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "0"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "0"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "0"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "0"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "0"]; }

	if (msg.content.startsWith("/attack")) { return ["n attack roll (sword)", "2"]; }
    }

    else { 
	if (msg.content.startsWith("/str")) { return [" strength check", "-20"]; }
	if (msg.content.startsWith("/dex")) { return [" dexterity check", "-20"]; }
	if (msg.content.startsWith("/con")) { return [" constitution check", "-20"]; }
	if (msg.content.startsWith("/int")) { return ["n intelligence check", "-20"]; }
	if (msg.content.startsWith("/wis")) { return [" wisdom check", "-20"]; }
	if (msg.content.startsWith("/cha")) { return [" charisma check", "-20"]; }
	if (msg.content.startsWith("/acr")) { return ["n acrobatics check", "-20"]; }
	if (msg.content.startsWith("/ani")) { return ["n animal handling check", "-20"]; }
	if (msg.content.startsWith("/arc")) { return ["n arcana check", "-20"]; }
	if (msg.content.startsWith("/ath")) { return ["n athletics check", "-20"]; }
	if (msg.content.startsWith("/dec")) { return [" deception check", "-20"]; }
	if (msg.content.startsWith("/his")) { return [" history check", "-20"]; }
	if (msg.content.startsWith("/ins")) { return ["n insight check", "-20"]; }
	if (msg.content.startsWith("/inti")) { return ["n intimidation roll", "-20"]; }
	if (msg.content.startsWith("/inv")) { return ["n investigation check", "20"]; }
	if (msg.content.startsWith("/med")) { return [" medicine check", "-20"]; }
	if (msg.content.startsWith("/nat")) { return [" nature check", "-20"]; } 	
	if (msg.content.startsWith("/perf")) { return [" performance check", "-20"]; }
	if (msg.content.startsWith("/pers")) { return [" persuation roll", "-20"]; }
	if (msg.content.startsWith("/per")) { return [" perception check", "-20"]; }
	if (msg.content.startsWith("/rel")) { return [" religion roll", "-20"]; }
	if (msg.content.startsWith("/sle")) { return [" sleight of hand roll", "-20"]; }
	if (msg.content.startsWith("/ste")) { return [" stealth roll", "-20"]; }
	if (msg.content.startsWith("/sur")) { return [" survival check", "-20"]; }

	if (msg.content.startsWith("/init")) { return ["n initiative roll", "-20"]; }
    }

}
