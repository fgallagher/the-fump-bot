// Plugin Control Plugin

var listRegex = /^\/plugins?$/i;
var controlRegex = /^\/plugin (add|enable|delete|disable) (.*?)$/i;

function PluginControl() {
	this.name = "Plugin Control";
	this.version = "1.0";

	this.handleMessage = function ( msg, accessableData ) {
		if( accessableData.hasOwnProperty('plugins') ) {
			var plugins = accessableData['plugins'];
		
			if( listRegex.test(msg.content) ) {
					var message = "**Fump Plugins**\n\n";
			
					for( var pluginKey in plugins ) {
						message
						+=	"**" + pluginKey + ":**\n"
						+	"\t__" + plugins[pluginKey].name + "__ "
						+	"v" + plugins[pluginKey].version + ", "
						+	(plugins[pluginKey].enabled ? "enabled" : "disabled") + "\n";
					}

					msg.channel.sendMessage(message);
			} else if( controlRegex.test(msg.content) ) {
				var controlArgs = controlRegex.exec(msg.content);
			
				var controlAction = controlArgs[1];
				var pluginid = controlArgs[2];
			
				var message = "";
				
				if( plugins.hasOwnProperty(pluginid) ) {
					switch( controlAction ) {
						case "delete" :
						case "disable" :
							plugins[pluginid].enabled = false;
							
							message = "Plugin _" + plugins[pluginid].name + "_ has been disabled.";
							
							break;
						case "add" :
						case "enable" :
							plugins[pluginid].enabled = true;

							message = "Plugin _" + plugins[pluginid].name + "_ has been enabled.";
							
							break;
						default:
							msg.channel.sendMessage("Plugin action *" + controlAction + "* is not defined." );
					}
					
					msg.channel.sendMessage(message);
				} else {
					msg.channel.sendMessage("Plugin *" + pluginid + "* does not exist.");
				}
			}
		} else {
			console.log("ERROR: plugins JSON not available from the fump.")
		}
	};
}

module.exports = PluginControl;


