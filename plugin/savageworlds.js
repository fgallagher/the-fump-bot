// Savage Worlds Data
var fs = require("fs");

var injuryTableRegex = /^(?:!|\/)injury(?:s|table)\b/i;
var hindranceRegex = /^(?:(?:!|\/)hindrance )(.*)/i;
var edgeRegex = /^(?:(?:!|\/)edge )(.*)/i;

var savageData = JSON.parse(fs.readFileSync("./plugin/savageworlddata.json"));

var injuryTable = savageData.injurytable;
var hindrances = savageData.hindrances;
var edges = savageData.edges;

function SavageWorldsPlugin() {
	this.name = "Savage Worlds Reference";
	this.version = "1.0";
	
	this.handleMessage = function ( msg ) {
		if ( injuryTableRegex.test(msg.content) ) {
			var message = "__**Injury Table**__\n\n";
			
			for( i in injuryTable ) {
				var injury = injuryTable[i];
				
				message += "**" + injury.roll + "**" + Array((injury.roll.length < 2 ? 4 : (injury.roll.length < 4 ? 3 : 2) )).join("\t") + "**" + injury.location + ":** " + injury.description + "\n";
				
				if( injury.hasOwnProperty('extra') ) {
					for( h in injury.extra ) {
						var detail = injury.extra[h];
						
						message += "\t\t\t\t\t*" + detail.roll + " " + detail.name + ":* " + detail.description + "\n";
					}
				}
				
				message += "\n";
			}

			msg.channel.sendMessage(message);
		} else if ( hindranceRegex.test(msg.content) ) {
			var hindranceName = hindranceRegex.exec(msg.content)[1];

			var matchedHindrances = [];

			for( var key in hindrances ) {
				if(hindrances.hasOwnProperty(key)) {
		
					if(key.toLowerCase().includes(hindranceName.toLowerCase())) {
						if(key.toLowerCase() == hindranceName.toLowerCase()) {
							matchedHindrances = [key];
							break;
						}
						matchedHindrances.push(key);
					}
				}
			}

			if( matchedHindrances.length > 1 ) {
				msg.channel
					.sendMessage("Which hindrance do you want information on?  "
						+ matchedHindrances
							.join(', ')
							.replace(/(, [^,]+$)/, function myf(x) {return ' or' + x;}));
			} else if( matchedHindrances.length == 0 ) {
				msg.channel.sendMessage("No hindrance found for '" + hindranceName + "'.");
			} else {
				var hindranceName = matchedHindrances[0];
				var hindrance = hindrances[hindranceName]
	
				var message = "__**" + hindranceName + " (" + hindrance.severity + ")**__\n";
	
				if( hindrance.hasOwnProperty('description') ) { message += "\t" + hindrance['description'] + "\n"; }
	
				msg.channel.sendMessage(message);
			}
		} else if ( edgeRegex.test(msg.content) ) {
			var edgeName = edgeRegex.exec(msg.content)[1];

			var matchedEdges = [];

			for( var key in edges ) {
				if(edges.hasOwnProperty(key)) {
		
					if(key.toLowerCase().includes(edgeName.toLowerCase())) {
						if(key.toLowerCase() == edgeName.toLowerCase()) {
							matchedEdges = [key];
							break;
						}
						matchedEdges.push(key);
					}
				}
			}

			if( matchedEdges.length > 1 ) {
				msg.channel
					.sendMessage("Which edge do you want information on?  "
						+ matchedEdges
							.join(', ')
							.replace(/(, [^,]+$)/, function myf(x) {return ' or' + x;}));
			} else if( matchedEdges.length == 0 ) {
				msg.channel.sendMessage("No hindrance found for '" + edgeName + "'.");
			} else {
				var edgeName = matchedEdges[0];
				var edge = edges[edgeName]
	
				var message = "__**" + edgeName + "**__\n";
	
				if( edge.hasOwnProperty('requirements') ) { message += "\t**Requirements:** " + edge['requirements'] + "\n"; }
				if( edge.hasOwnProperty('description') ) { message += "\t" + edge['description'] + "\n"; }
	
				msg.channel.sendMessage(message);
			}
		}
	};
}

module.exports = SavageWorldsPlugin;
