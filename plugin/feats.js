// Feats Plugin
var fs = require("fs");

var pluginRegex = /^(?:(?:!|\/)feat )(.*)/i;

function FeatPlugin() {
	this.name = "D&D 5e Feats ";
	this.version = "1.0";
	
	this.feats = JSON.parse(fs.readFileSync("./plugin/feats.json"));
	
	this.handleMessage = function ( msg ) {
		if( pluginRegex.test(msg.content) ) {
			var featName = pluginRegex.exec(msg.content)[1];
	
			var matchedFeats = [];
	
			for( var key in this.feats ) {
				if(this.feats.hasOwnProperty(key)) {
			
					if(key.toLowerCase().includes(featName.toLowerCase())) {
						if(key.toLowerCase() == featName.toLowerCase()) {
							matchedFeats = [key];
							break;
						}
						matchedFeats.push(key);
					}
				}
			}
	
			if( matchedFeats.length > 1 ) {
				msg.channel
					.sendMessage("Which feat do you want information on?  "
						+ matchedFeats
							.join(', ')
							.replace(/(, [^,]+$)/, function myf(x) {return ' or' + x;}));
			} else if( matchedFeats.length == 0 ) {
				msg.channel.sendMessage("No feat found for '" + featName + "'.");
			} else {
				var featName = matchedFeats[0];
				var feat = this.feats[featName]
		
				var message = "__**" + featName + "**__\n\n";
		
				if( feat.hasOwnProperty('prerequisite') ) { message += "**Prerequisite:** " + feat['prerequisite'] + "\n\n"; }

				message	+=
					"**Description:** " + feat['description'];
		
				msg.channel.sendMessage(message);
			}
		}
	};
};

module.exports = FeatPlugin;
