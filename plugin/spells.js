// Spells
var fs = require("fs");

var pluginRegex = /^(?:(?:!|\/)spell )(.*)/i;
var spells = JSON.parse(fs.readFileSync("./plugin/spells.json"));

function SpellPlugin() {
	this.name = "D&D 5e Spell List";
	this.version = "1.0";
	
	this.handleMessage = function ( msg ) {
		if ( pluginRegex.test(msg.content) ) {
			var spellName = pluginRegex.exec(msg.content)[1];
	
			var matchedSpells = [];
	
			for( var key in spells ) {
				if(spells.hasOwnProperty(key)) {
			
					if(key.toLowerCase().includes(spellName.toLowerCase())) {
						if(key.toLowerCase() == spellName.toLowerCase()) {
							matchedSpells = [key];
							break;
						}
						matchedSpells.push(key);
					}
				}
			}
	
			if( matchedSpells.length > 1 ) {
				msg.channel
					.sendMessage("Which spell do you want information on?  "
						+ matchedSpells
							.join(', ')
							.replace(/(, [^,]+$)/, function myf(x) {return ' or' + x;}));
			} else if( matchedSpells.length == 0 ) {
				msg.channel.sendMessage("No spell found for '" + spellName + "'.");
			} else {
				var spellName = matchedSpells[0];
				var spell = spells[spellName]
		
				var message = "__**" + spellName + "**__\n\n";
		
				if( spell.hasOwnProperty('casting_time') ) { message += "**Casting Time:** " + spell['casting_time'] + "\n"; }
				if( spell.hasOwnProperty('components') ) { message += "**Components:** " + spell['components'] + "\n"; }
				if( spell.hasOwnProperty('material') ) { message += "**Material:** " + spell['material'] + "\n"; }
				if( spell.hasOwnProperty('duration') ) { message += "**Duration:** " + spell['duration'] + "\n"; }
				if( spell.hasOwnProperty('level') ) { message += "**Level:** " + spell['level'] + "\n"; }
				if( spell.hasOwnProperty('range') ) { message += "**Range:** " + spell['range'] + "\n"; }
				if( spell.hasOwnProperty('school') ) { message += "**School:** " + spell['school'] + "\n"; }
				if( spell.hasOwnProperty('class') ) { message += "**Class:** " + spell['class'] + "\n"; }

				message	+=
					"**Description:**\n    " + spell['description'];
		
				if( spell.hasOwnProperty('higher_level') ) {
					message += "\n**Higher Level:**\n    " + spell['higher_level'];
				}

				msg.channel.sendMessage(message);
			}
		}
	};
};

module.exports = SpellPlugin;
