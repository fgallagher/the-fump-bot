// Dice Plugin
var DiceExpression = require('./dice-roller.js');
var DiceRoller = new DiceExpression.DiceRoller();

var diceRegex = /^(?:(?:!|\/)r(?:oll)? )(.*?)(#.*)?$/i;

function DicePlugin() {
	this.name = "The Fump Die Roller";
	this.version = "2.0";
	
	this.handleMessage = function ( msg ) {
		if ( diceRegex.test(msg.content) ) {
			var mentionName = "<@!" + msg.author.id + ">";
			var diceMatch = diceRegex.exec(msg.content);
			var rollString = diceMatch[1].replace(/ /g, '');
			var rollComment = diceMatch[2];
			
			try {
				var dRoll = DiceRoller.roll( rollString );
				
				var message = mentionName + ": ";
	
				if( rollComment != undefined ) {
					message += " _" + rollComment.substring(1).trim() + ":_ ";
				}

				message += dRoll.toString().replace(/^([^:]*):/, "`$1` =").replace(/(\d*)$/, "**$1**");

				msg.channel.sendMessage( message );
			} catch(err) {
				console.log("Did not understand roll request: '" + rollString + "'");
				console.log(err);
				msg.channel.sendMessage( '_farts_' );
				msg.channel.sendMessage( msg.author + ', your roll defies all logic.' );
			}
		}		
	};
}

module.exports = DicePlugin;