// Savage Worlds Action Deck Plugin

var deck = new Array();

var adRegex = /^\/(ad|(?:action)?deck) (shuffle(?:all)?|collect|stat(?:us)?)\b(.*?)$/i;
var personalCommandsRegex = /^\/(draw|discard)\b(?:\s+(\d))?/i;
var transitiveCommandsRegex = /^\/(deal)\b(?:\s+([^\s]+))(?:\s+(\d))?/i;

var allowedRoleName = '@gamemaster';

function ActionDeck() {
	this.name = "Savage Worlds Action Deck";
	this.version = "1.0";
	
	this.drawDeck = buildDeck();
	this.discardDeck = new Array();
	this.looseCards = new Array();
	
	this.handleMessage = function ( msg, accessableData ) {
		var match;
		var command;
		var args = new Array();

		if( personalCommandsRegex.test( msg.content ) ) {
			match = personalCommandsRegex.exec( msg.content );
			
			command = match[1];
			args.push(match[2]);
		} else if ( adRegex.test( msg.content ) ) {
			match = adRegex.exec(msg.content);
			
			command = match[2];
		} else if ( transitiveCommandsRegex.test( msg.content ) ) {
			match = transitiveCommandsRegex.exec( msg.content );
			
			command = match[1];
			args = match.slice(2,4);
		}
		
		if( match != null ) {
			var mentionName = "<@!" + msg.author.id + ">";

			switch ( command ) {
				case 'draw':
					var count = args[0] ? args[0] : 1;
				
					for( i = 1 ; i <= count ; i += 1 ) {
						var id = msg.author.id;
						this.draw(msg, [msg.author]);
					}

					break;
				case 'discard':
					var toDiscard = new Array();
				
					for (i = this.looseCards.length - 1; i >= 0; i -= 1) {
						if( this.looseCards[i].user.id === msg.author.id ) {
							toDiscard.push(this.looseCards.splice(i,1).card);
						}
					}
				
					if( toDiscard.length > 0 ) {
						this.discardDeck = this.discardDeck.concat(toDiscard);
					
						msg.channel.sendMessage( mentionName + " discards " + toDiscard.length + " cards." );
					} else {
						msg.channel.sendMessage( mentionName + " has no cards to discard." );
					}
					break;
				case 'shuffle':
					if( msg.member.roles.find('name', allowedRoleName) ) {
						this.reshuffleDiscard(msg);
					} else {
						msg.channel.sendMessage( mentionName + " does not have privledges to shuffle." );
					}
					break;
				case 'shuffleall':
					if( msg.member.roles.find('name', allowedRoleName) ) {
						this.shuffleAll(msg);
					} else {
						msg.channel.sendMessage( mentionName + " does not have privledges to shuffle." );
					}
					break;
				case 'collect':
					if( msg.member.roles.find('name', allowedRoleName) ) {
						this.collectCards(msg);
					} else {
						msg.channel.sendMessage( mentionName + " does not have privledges to collect." );
					}
					break;
				case 'status':
					this.printStatus(msg);
					break;
				case 'deal':
					if( msg.member.roles.find('name', allowedRoleName) ) {
						this.draw(msg, msg.mentions.users.array());
					} else {
						msg.channel.sendMessage( mentionName + " does not have privledges to deal." );
					}
					
					break;
			}
		}
	};
	
	this.printStatus = function (msg) {
		if( msg != null ) {
			var message = 
				"**Savage Worlds Action Deck**```"
				+ "\nDraw Deck:    " + this.drawDeck.length
				+ "\nDiscard Deck: " + this.discardDeck.length
				+ "\nLoose Cards:  " + this.looseCards.length
				+ '```';

			if( this.looseCards.length > 0 ) {
				this.looseCards.sort(function(a,b) {
					return a.user.username.localeCompare(b.user.username);
				});
			
				var userid = 0;
			
				for( var i in this.looseCards ) {
					var looseCard = this.looseCards[i];

					if( userid != looseCard.user.id ) {
						if( userid > 0 ) {
							message += "\n";
						}
					
						userid = looseCard.user.id;
					
						message += "<@!" + userid + ">: ";
					}
				
					message += "[" + looseCard.card.toMessage() + "] ";
				}
			}
			
			msg.channel.sendMessage(message).catch(function (e) {
				console.log("Failed to send Action Deck status.");
				console.log( e );
			});
		}
	};

	this.shuffle = function (msg) {
	    var currentIndex = this.drawDeck.length, temporaryValue, randomIndex;

	    // While there remain elements to shuffle...
	    while (0 !== currentIndex) {

	      // Pick a remaining element...
	      randomIndex = Math.floor(Math.random() * currentIndex);
	      currentIndex -= 1;

	      // And swap it with the current element.
	      temporaryValue = this.drawDeck[currentIndex];
	      this.drawDeck[currentIndex] = this.drawDeck[randomIndex];
	      this.drawDeck[randomIndex] = temporaryValue;
	    }
		
		if( msg != null ) {
			msg.channel.sendMessage( "Shuffling the action deck." );
		}
	};
	
	this.reshuffleDiscard = function (msg) {
		this.drawDeck = this.drawDeck.concat(this.discardDeck);
		this.discardDeck = new Array();

		if( msg != null ) {
			msg.channel.sendMessage( "Combining the discard pile into the draw deck." );
		}
		
		this.shuffle(msg);
	};
	
	this.shuffleAll = function (msg) {
		this.collectCards(msg);
		this.reshuffleDiscard(msg);
	};
	
	this.collectCards = function (msg) {
		if( msg != null ) {
			msg.channel.sendMessage( "Collecting all cards into the discard pile." );
		}
		
		while( this.looseCards.length > 0 ) {
			this.discardDeck.push(this.looseCards.pop().card);
		}
	};
	
	this.draw = function (msg, userCollection) {
		for( var i in userCollection ) {
			if( this.drawDeck.length === 0 ) {
				this.reshuffleDiscard();
			}
		
			var card = this.drawDeck.pop();
			this.looseCards.push({
				'user': userCollection[i],
				'card': card
			});
		
			msg.channel.sendMessage( "<@!" + userCollection[i].id + ">" + " draws the " + card.toMessage() );
		}
		
		if( this.drawDeck.length === 0 ) {
			this.reshuffleDiscard();
		}
	};

	this.shuffle();
}

function Card(val, suite) {
	this.value = {
		'value' : val.value,
		'sort' : val.sort
	};
	
	this.suite = {
		'abrev' : suite.abrev,
		'name' : suite.name
	};
	
	this.toString = function() {
		return this.value.value + " of " + this.suite.name;
	}
	
	this.toMessage = function() {
		return "**" + this.value.value + ":" + this.suite.name + ":**";
	}
}

function buildDeck() {
	var deck = new Array();
	
	var cardValues = [
		{
			'value' : 2,
			'sort' : 2
		},
		{
			'value' : 3,
			'sort' : 3
		},
		{
			'value' : 4,
			'sort' : 4
		},
		{
			'value' : 5,
			'sort' : 5
		},
		{
			'value' : 6,
			'sort' : 6
		},
		{
			'value' : 7,
			'sort' : 7
		},
		{
			'value' : 8,
			'sort' : 8
		},
		{
			'value' : 9,
			'sort' : 9
		},
		{
			'value' : 10,
			'sort' : 10
		},
		{
			'value' : 'J',
			'sort' : 11
		},
		{
			'value' : 'Q',
			'sort' : 12
		},
		{
			'value' : 'K',
			'sort' : 13
		},
		{
			'value' : 'A',
			'sort' : 14
		}
	];
	var cardSuites = [
		{
			abrev: 'c',
			name: 'clubs'
		},
		{
			abrev: 'd',
			name: 'diamonds'
		},
		{
			abrev: 'h',
			name: 'hearts'
		},
		{
			abrev: 's',
			name: 'spades'
		}
	];
	
	for ( suite of cardSuites ) {
		for ( cval of cardValues ) {
			deck.push( new Card(cval, suite) );
		}
	}
		
	deck.push( new Card({'value' : 'Joker','sort' : 15}, {'abrev' : 'bj','name' : 'black_joker'} ) );
	deck.push( new Card({'value' : 'Joker','sort' : 15}, {'abrev' : 'wj','name' : 'black_joker'} ) );
	
	return deck;
}

// Draw
	// Encounters
// Chase rules
// Dramatic Tasks rules

module.exports = ActionDeck;








