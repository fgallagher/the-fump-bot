# **The Fump**

*an RPG bot for Discord*

---

The Fump is a Discord bot whos primary directive is to serve man with Tabletop RPG tasks.  The Fump's core feature is to load and pass messages to plugins.  The plugins handle messages and perform specific functions


## Plugins

### 1. The Fump Dice Roller

This plugin allows users to roll dice using the RPG Dice Roller by GreenImp - http://rpg.greenimp.co.uk/dice-roller/.

Use `/roll` or `/r` to pass the dice code to The Fump.

#### Usage

`/roll <die-code> (<# comment>)`

#### Example Dice Commands

* `/roll d20+5 # Sword Attack`
* `/roll 2d20+5-L # Attack roll with Advantage`
* `/roll 2d6!! # Exploding damage!`

---

### 2. D&D 5e Spell List

This plugin allows users to display details of D&D 5e spells.


#### Usage

`/spell <name of spell>`

#### Example Spell Command

* `/spell Magic Missile`

---

### 3. D&D 5e Feat List

This plugin allows users to display details of D&D 5e feats.


#### Usage

`/feat <name of feat>`

#### Example Feat Command

* `/feat War Caster`

---

### 4. Fump Macros

This plugin allows users to save macros that The Fump will understand.  The most common use case for this is for saving dice rolling codes.

#### Macro Commands

* **add**

    Adds a new macro.
    
    _Usage:_ `/macro add kill /roll d20+5 # Sword Attack`

* **delete**

	Deletes an existing macro.
    
	_Usage:_ `/macro delete kill`
    
* **list**

	Lists all saved macros.
    
    _Usage:_ `/macro list`

---

