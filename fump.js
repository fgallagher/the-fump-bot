// The Fump
//   Discord bot assisting in RPGs
//
//   Author: Frank Gallagher - frank@mjolnirgroup.com
//   site:   https://gitlab.com/fgallagher/the-fump-bot/

// Set up Discord client
var Discord = require("discord.js");
var client = new Discord.Client();

// Array of plugins
var pluginList = require('./plugin/plugins.json');
var plugins = {};
var sharedData = {
	'plugins' : pluginList
};

loadPlugins();

// Configureation file
const config = require("./config.json");

// Message Handler
client.on("message", msg => {
	handleMessage(msg);
});

// Message Update Handlers
client.on("messageUpdate", function(oldMsg, newMsg) {
	handleMessage(newMsg);
});

// Perform on launch
client.on('ready', () => {
	console.log('I am the fump!');
});

// Handle the message by looping through the plugins
function handleMessage( msg ) {
	for( var pluginKey in plugins ) {
		try {
			// plugin.handleMessage(msg, sharedData);
			if( pluginList[pluginKey].enabled ) {
				plugins[pluginKey].handleMessage(msg, sharedData);
			}
		} catch(e) {
			console.log("Plugin failed: " + e);
			console.log(e);
		}
	}
}

// Load plugins from the plugin dictated by the plugin json file
function loadPlugins() {
	console.log('Loading plugins...');
	
	for( var pluginName in pluginList ) {
		var plugin = pluginList[pluginName];
		
		if( plugin.enabled ) {
			try {
				var thePlugin = require(plugin.path);
				
				var pluginObj = new thePlugin();
				
				plugins[pluginName] = pluginObj;
				
				pluginList[pluginName].name = pluginObj.name;
				pluginList[pluginName].version = pluginObj.version;
				
				console.log( '  ' + plugins[pluginName].name + ' v' + plugins[pluginName].version );
			} catch(e) {
				console.log( '  ' + pluginName + ' FAILED TO LOAD.' );
				console.log( '    ' + e );
				console.log( e );
			}
		}
	}
	
	console.log('');
}

client.login(config.token).catch( function(e) {
	console.log("Failed to login: " + e);
});
